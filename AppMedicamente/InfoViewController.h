//
//  InfoViewController.h
//  AppMedicamente
//
//  Created by Alex on 31/03/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *Button_Back;
- (IBAction)Action_Button_Back:(id)sender;
- (IBAction)Action_Button_Web:(id)sender;

@end

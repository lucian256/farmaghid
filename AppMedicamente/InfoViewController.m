//
//  InfoViewController.m
//  AppMedicamente
//
//  Created by Alex on 31/03/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "InfoViewController.h"

@implementation InfoViewController

float cornerRadiusInfo = 5;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _Button_Back.layer.cornerRadius = cornerRadiusInfo;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Action_Button_Back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)Action_Button_Web:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.farmaghid.ro"]];
}
@end

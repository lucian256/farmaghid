//
//  ViewController.h
//  AppMedicamente
//
//  Created by Alex on 24/03/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView_scroll;

@property (weak, nonatomic) IBOutlet UIButton *Button_Medicament;
@property (weak, nonatomic) IBOutlet UIButton *Button_Greutate;
@property (weak, nonatomic) IBOutlet UIButton *Button_Administrari;
@property (weak, nonatomic) IBOutlet UIButton *Button_Calculeaza;

@property (weak, nonatomic) IBOutlet UITextView *TextView_Log;
- (IBAction)Action_Button_Medicament:(id)sender;
- (IBAction)Action_Button_Greutate:(id)sender;
- (IBAction)Action_Button_Administrari:(id)sender;
- (IBAction)Action_Button_Calculeaza:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *Label_Medicament;
@property (weak, nonatomic) IBOutlet UILabel *Label_Greutate;
@property (weak, nonatomic) IBOutlet UILabel *Label_Administrare;
@property (weak, nonatomic) IBOutlet UILabel *Label_Result_Medicament;
@property (weak, nonatomic) IBOutlet UILabel *Label_Result_Greutate;
@property (weak, nonatomic) IBOutlet UILabel *Label_Result_Doze;
@property (weak, nonatomic) IBOutlet UILabel *Label_Result_Doze24;

@property (weak, nonatomic) IBOutlet UILabel *Label_Result_Dozemg;
@property (weak, nonatomic) IBOutlet UILabel *Label_Result_Dozeml;
@property (weak, nonatomic) IBOutlet UIButton *Button_Info;
@property (weak, nonatomic) IBOutlet UIView *View_Medicament;
@property (weak, nonatomic) IBOutlet UIView *View_Greutate;
@property (weak, nonatomic) IBOutlet UIView *View_Administrari;
@property (weak, nonatomic) IBOutlet UIView *View_ResultMedicament;
@property (weak, nonatomic) IBOutlet UIView *VIew_ResultGreutate;
@property (weak, nonatomic) IBOutlet UIView *View_ResultDoze;
@property (weak, nonatomic) IBOutlet UIView *View_ResultDoze24;
@property (weak, nonatomic) IBOutlet UIView *View_ResultDozemg;
@property (weak, nonatomic) IBOutlet UIView *View_ResultDozeml;
@property (weak, nonatomic) IBOutlet UIView *View_Contain;
- (IBAction)Action_Button_Info:(id)sender;
@end


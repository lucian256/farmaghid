//
//  ViewController.m
//  AppMedicamente
//
//  Created by Alex on 24/03/15.
//  Copyright (c) 2015 Alex. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

NSMutableArray * tableData;

NSMutableArray * tableData_1;
NSMutableArray * tableData_2;
NSMutableArray * tableData_3;
NSMutableDictionary * medTableData;

int currentDropdown = 0;

int selectedPositionForDropdown_1 = -1;
int selectedPositionForDropdown_2 = -1;
int selectedPositionForDropdown_3 = -1;

UITableView * dropDownTableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initPage];
    
    [self roundStuff];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //_TextView_Log.text = @"";
    //[_TextView_Log scrollRangeToVisible:NSMakeRange(0, 0)];
}

- (IBAction)Action_Button_Medicament:(id)sender
{
        if(currentDropdown == 1)
        {
            [self hideDropdown];
        }
        else
        {
            currentDropdown = 1;
            [self moveDropdownToButton];
            [self showDropDown];
        }
}

- (IBAction)Action_Button_Greutate:(id)sender
{
    if(currentDropdown == 2)
    {
        [self hideDropdown];
    }
    else
    {
        currentDropdown = 2;
        [self moveDropdownToButton];
        [self showDropDown];
    }
}

- (IBAction)Action_Button_Administrari:(id)sender
{
    if(currentDropdown == 3)
    {
        [self hideDropdown];
    }
    else
    {
        currentDropdown = 3;
        [self moveDropdownToButton];
        [self showDropDown];
    }
}

- (IBAction)Action_Button_Calculeaza:(id)sender
{
    if(selectedPositionForDropdown_1 != -1 && selectedPositionForDropdown_2 != -1 && selectedPositionForDropdown_3 != -1)
    {
        [self Calculeaza];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Atentie"
                                                       message: @"Toate campurile trebuie sa fie completate!"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"otherButtonTitles:nil];
        
        alert.tag = 1;
        alert.show;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return tableData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"DropDownCell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"DropDownCell"];
    }
    if(currentDropdown == 1)
    {
        cell.textLabel.text = [tableData objectAtIndex: indexPath.row];
    }
    
    if(currentDropdown == 2)
    {
        NSString * text = [NSString stringWithFormat:@"%@ kg", [tableData objectAtIndex: indexPath.row]];
        cell.textLabel.text = text;
    }
    
    if(currentDropdown == 3)
    {
        NSString * text = [NSString stringWithFormat:@"%@ administrari", [tableData objectAtIndex: indexPath.row]];
        cell.textLabel.text = text;
    }
    
    
    
    [cell.textLabel sizeToFit];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(currentDropdown == 1)
    {
        selectedPositionForDropdown_1 = indexPath.row;
        NSString * title = [tableData objectAtIndex:indexPath.row];
        _Label_Medicament.text = title;
        //[_Button_Medicament setTitle:title forState:UIControlStateNormal];
        [self hideDropdown];
    }

    if(currentDropdown == 2)
    {
        selectedPositionForDropdown_2 = indexPath.row;
        NSString * title = [tableData objectAtIndex:indexPath.row];
        _Label_Greutate.text = title;
//        [_Button_Greutate setTitle:title forState:UIControlStateNormal];
        [self hideDropdown];
    }

    if(currentDropdown == 3)
    {
        selectedPositionForDropdown_3 = indexPath.row;
        NSString * title = [tableData objectAtIndex:indexPath.row];
        _Label_Administrare.text = title;
//        [_Button_Administrari setTitle:title forState:UIControlStateNormal];
        [self hideDropdown];
    }
}

float cornerRadius = 5;

- (void) roundStuff
{

    _View_Medicament.layer.borderWidth = 1;
    _View_Medicament.layer.borderColor = [UIColor grayColor].CGColor;
    
    _View_Greutate.layer.borderWidth = 1;
    _View_Greutate.layer.borderColor = [UIColor grayColor].CGColor;
    
    _View_Administrari.layer.borderWidth = 1;
    _View_Administrari.layer.borderColor = [UIColor grayColor].CGColor;
    
    _View_Medicament.layer.cornerRadius = cornerRadius;
    _View_Greutate.layer.cornerRadius = cornerRadius;
    _View_Administrari.layer.cornerRadius = cornerRadius;
    _Button_Calculeaza.layer.cornerRadius = cornerRadius;
    _View_ResultMedicament.layer.cornerRadius = cornerRadius;
    _VIew_ResultGreutate.layer.cornerRadius = cornerRadius;
    _View_ResultDoze.layer.cornerRadius = cornerRadius;
    _View_ResultDoze24.layer.cornerRadius = cornerRadius;
    _View_ResultDozemg.layer.cornerRadius = cornerRadius;
    _View_ResultDozeml.layer.cornerRadius = cornerRadius;
    
    _View_Contain.layer.cornerRadius = cornerRadius;
    
//    [label sizeToFit];
    
    [_Label_Medicament sizeToFit];
    [_Label_Greutate sizeToFit];
    [_Label_Administrare sizeToFit];
    [_Label_Result_Medicament sizeToFit];
    [_Label_Result_Greutate sizeToFit];
    [_Label_Result_Doze sizeToFit];
    [_Label_Result_Doze24 sizeToFit];
    [_Label_Result_Dozemg sizeToFit];
    [_Label_Result_Dozeml sizeToFit];

    _Button_Info.layer.cornerRadius = cornerRadius;
    
    dropDownTableView.layer.borderWidth = 1;
    dropDownTableView.layer.borderColor = [UIColor grayColor].CGColor;

}

- (void) populate
{
    medTableData = [NSMutableDictionary new];
    //1#2#3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40#41#42#43#44#45#46#47#48#49#50
    //2#3#4
    //@"amoxicilina 125 mg per 5ml"
    NSMutableDictionary * med0 = [NSMutableDictionary new];
    [med0 setValue:@"amoxicilina 125 mg per 5ml" forKey:@"name"];
    [med0 setValue:@"50" forKey:@"doza"];
    [med0 setValue:@"125" forKey:@"mg"];
    [med0 setValue:@"5" forKey:@"ml"];
    [med0 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40#41#42#43#44#45#46#47#48#49#50" forKey:@"greutate"];
    [med0 setValue:@"2#3#4" forKey:@"administrari"];
    
    [medTableData setObject:med0 forKey:@"amoxicilina 125 mg per 5ml"];
    
    //@"amoxicilina 250 mg per 5ml"
    NSMutableDictionary * med1 = [NSMutableDictionary new];
    [med1 setValue:@"amoxicilina 250 mg per 5ml" forKey:@"name"];
    [med1 setValue:@"50" forKey:@"doza"];
    [med1 setValue:@"250" forKey:@"mg"];
    [med1 setValue:@"5" forKey:@"ml"];
    [med1 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40#41#42#43#44#45#46#47#48#49#50" forKey:@"greutate"];
    [med1 setValue:@"2#3#4" forKey:@"administrari"];
    
    [medTableData setObject:med1 forKey:@"amoxicilina 250 mg per 5ml"];

    //@"amoxicilina+clav 156 mg per 5ml"
    NSMutableDictionary * med2 = [NSMutableDictionary new];
    [med2 setValue:@"amoxicilina+clav 156 mg per 5ml" forKey:@"name"];
    [med2 setValue:@"50" forKey:@"doza"];
    [med2 setValue:@"125" forKey:@"mg"];
    [med2 setValue:@"5" forKey:@"ml"];
    [med2 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med2 setValue:@"3" forKey:@"administrari"];
    [medTableData setObject:med2 forKey:@"amoxicilina+clav 156 mg per 5ml"];

    //@"amoxicilina+clav 228 mg per 5ml"
    NSMutableDictionary * med3 = [NSMutableDictionary new];
    [med3 setValue:@"amoxicilina+clav 228 mg per 5ml" forKey:@"name"];
    [med3 setValue:@"40" forKey:@"doza"];
    [med3 setValue:@"200" forKey:@"mg"];
    [med3 setValue:@"5" forKey:@"ml"];
    [med3 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med3 setValue:@"2#3" forKey:@"administrari"];
    [medTableData setObject:med3 forKey:@"amoxicilina+clav 228 mg per 5ml"];

    //@"amoxicilina+clav 312 mg per 5ml"
    NSMutableDictionary * med4 = [NSMutableDictionary new];
    [med4 setValue:@"amoxicilina+clav 312 mg per 5ml" forKey:@"name"];
    [med4 setValue:@"50" forKey:@"doza"];
    [med4 setValue:@"250" forKey:@"mg"];
    [med4 setValue:@"5" forKey:@"ml"];
    [med4 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med4 setValue:@"3" forKey:@"administrari"];
    [medTableData setObject:med4 forKey:@"amoxicilina+clav 312 mg per 5ml"];
    
    //@"amoxicilina+clav 457 mg per 5ml"
    NSMutableDictionary * med5 = [NSMutableDictionary new];
    [med5 setValue:@"amoxicilina+clav 457 mg per 5ml" forKey:@"name"];
    [med5 setValue:@"40" forKey:@"doza"];
    [med5 setValue:@"400" forKey:@"mg"];
    [med5 setValue:@"5" forKey:@"ml"];
    [med5 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med5 setValue:@"2#3" forKey:@"administrari"];
    [medTableData setObject:med5 forKey:@"amoxicilina+clav 457 mg per 5ml"];
    
    //@"amoxicilina+clav 642 mg per 5ml"
    NSMutableDictionary * med6 = [NSMutableDictionary new];
    [med6 setValue:@"amoxicilina+clav 642 mg per 5ml" forKey:@"name"];
    [med6 setValue:@"90" forKey:@"doza"];
    [med6 setValue:@"600" forKey:@"mg"];
    [med6 setValue:@"1" forKey:@"ml"];
    [med6 setValue:@"8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med6 setValue:@"2" forKey:@"administrari"];
    [medTableData setObject:med6 forKey:@"amoxicilina+clav 642 mg per 5ml"];
    
    //@"cefalexin 125 mg per 5 ml"
    NSMutableDictionary * med7 = [NSMutableDictionary new];
    [med7 setValue:@"cefalexin 125 mg per 5 ml" forKey:@"name"];
    [med7 setValue:@"50" forKey:@"doza"];
    [med7 setValue:@"125" forKey:@"mg"];
    [med7 setValue:@"5" forKey:@"ml"];
    [med7 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40#41#42#43#44#45#46#47#48#49#50" forKey:@"greutate"];
    [med7 setValue:@"2#3#4" forKey:@"administrari"];
    [medTableData setObject:med7 forKey:@"cefalexin 125 mg per 5 ml"];
    
    //@"cefalexin 250 mg per 5 ml"
    NSMutableDictionary * med8 = [NSMutableDictionary new];
    [med8 setValue:@"cefalexin 250 mg per 5 ml" forKey:@"name"];
    [med8 setValue:@"50" forKey:@"doza"];
    [med8 setValue:@"250" forKey:@"mg"];
    [med8 setValue:@"5" forKey:@"ml"];
    [med8 setValue:@"3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40#41#42#43#44#45#46#47#48#49#50" forKey:@"greutate"];
    [med8 setValue:@"2#3#4" forKey:@"administrari"];
    [medTableData setObject:med8 forKey:@"cefalexin 250 mg per 5 ml"];
    
    //@"cefadroxil 125 mg per 5 ml"
    NSMutableDictionary * med9 = [NSMutableDictionary new];
    [med9 setValue:@"cefadroxil 125 mg per 5 ml" forKey:@"name"];
    [med9 setValue:@"40" forKey:@"doza"];
    [med9 setValue:@"125" forKey:@"mg"];
    [med9 setValue:@"5" forKey:@"ml"];
    [med9 setValue:@"7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med9 setValue:@"2" forKey:@"administrari"];
    [medTableData setObject:med9 forKey:@"cefadroxil 125 mg per 5 ml"];
    
    //@"cefadroxil 250 mg per 5 ml"
    NSMutableDictionary * med10 = [NSMutableDictionary new];
    [med10 setValue:@"cefadroxil 250 mg per 5 ml" forKey:@"name"];
    [med10 setValue:@"40" forKey:@"doza"];
    [med10 setValue:@"250" forKey:@"mg"];
    [med10 setValue:@"5" forKey:@"ml"];
    [med10 setValue:@"7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med10 setValue:@"2" forKey:@"administrari"];
    [medTableData setObject:med10 forKey:@"cefadroxil 250 mg per 5 ml"];
    
    //@"cefuroxim 125 mg per 5 ml"
    NSMutableDictionary * med11 = [NSMutableDictionary new];
    [med11 setValue:@"cefuroxim 125 mg per 5 ml" forKey:@"name"];
    [med11 setValue:@"15" forKey:@"doza"];
    [med11 setValue:@"125" forKey:@"mg"];
    [med11 setValue:@"5" forKey:@"ml"];
    [med11 setValue:@"7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med11 setValue:@"2" forKey:@"administrari"];
    [medTableData setObject:med11 forKey:@"cefuroxim 125 mg per 5 ml"];
    
    //@"claritromicina 125 mg per 5 ml"
    NSMutableDictionary * med12 = [NSMutableDictionary new];
    [med12 setValue:@"claritromicina 125 mg per 5 ml" forKey:@"name"];
    [med12 setValue:@"15" forKey:@"doza"];
    [med12 setValue:@"125" forKey:@"mg"];
    [med12 setValue:@"5" forKey:@"ml"];
    [med12 setValue:@"8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med12 setValue:@"2" forKey:@"administrari"];
    [medTableData setObject:med12 forKey:@"claritromicina 125 mg per 5 ml"];
    
    //@"claritromicina 250 mg per 5 ml"
    NSMutableDictionary * med13 = [NSMutableDictionary new];
    [med13 setValue:@"claritromicina 250 mg per 5 ml" forKey:@"name"];
    [med13 setValue:@"15" forKey:@"doza"];
    [med13 setValue:@"250" forKey:@"mg"];
    [med13 setValue:@"5" forKey:@"ml"];
    [med13 setValue:@"8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40" forKey:@"greutate"];
    [med13 setValue:@"2" forKey:@"administrari"];
    [medTableData setObject:med13 forKey:@"claritromicina 250 mg per 5 ml"];
    
    //@"cefaclor 125 mg per 5 ml"
    NSMutableDictionary * med14 = [NSMutableDictionary new];
    [med14 setValue:@"cefaclor 125 mg per 5 ml" forKey:@"name"];
    [med14 setValue:@"20" forKey:@"doza"];
    [med14 setValue:@"125" forKey:@"mg"];
    [med14 setValue:@"5" forKey:@"ml"];
    [med14 setValue:@"5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40#41#42#43#44#45#46#47#48#49#50" forKey:@"greutate"];
    [med14 setValue:@"3" forKey:@"administrari"];
    [medTableData setObject:med14 forKey:@"cefaclor 125 mg per 5 ml"];
    
    //@"cefaclor 250 mg per 5 ml"
    NSMutableDictionary * med15 = [NSMutableDictionary new];
    [med15 setValue:@"cefaclor 250 mg per 5 ml" forKey:@"name"];
    [med15 setValue:@"20" forKey:@"doza"];
    [med15 setValue:@"250" forKey:@"mg"];
    [med15 setValue:@"5" forKey:@"ml"];
    [med15 setValue:@"5#6#7#8#9#10#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#27#28#29#30#31#32#33#34#35#36#37#38#39#40#41#42#43#44#45#46#47#48#49#50" forKey:@"greutate"];
    [med15 setValue:@"3" forKey:@"administrari"];
    [medTableData setObject:med15 forKey:@"cefaclor 250 mg per 5 ml"];
    
    tableData_1 = [NSMutableArray arrayWithObjects:
                   @"amoxicilina 125 mg per 5ml",
                   @"amoxicilina 250 mg per 5ml",
                   @"amoxicilina+clav 156 mg per 5ml",
                   @"amoxicilina+clav 228 mg per 5ml",
                   @"amoxicilina+clav 312 mg per 5ml",
                   @"amoxicilina+clav 457 mg per 5ml",
                   @"amoxicilina+clav 642 mg per 5ml",
                   @"cefalexin 125 mg per 5 ml",
                   @"cefalexin 250 mg per 5 ml",
                   @"cefadroxil 125 mg per 5 ml",
                   @"cefadroxil 250 mg per 5 ml",
                   @"cefuroxim 125 mg per 5 ml",
                   @"claritromicina 125 mg per 5 ml",
                   @"claritromicina 250 mg per 5 ml",
                   @"cefaclor 125 mg per 5 ml",
                   @"cefaclor 250 mg per 5 ml",
                   nil];
    tableData_2 = [NSMutableArray new];

    for(int i = 3; i < 51;i++)
    {
        NSString * text = [NSString stringWithFormat:@"%d kg",i];
        [tableData_2 addObject: text];
    }

    tableData_3 = [NSMutableArray new];

}

- (void) showDropDown //:(int ) dropdownId
{
    self.ScrollView_scroll.scrollEnabled = false;
    int dropdownId = currentDropdown;
    if(dropdownId == 1)
    {
        tableData = tableData_1;
        
        selectedPositionForDropdown_3 = -1;
        [tableData_3 removeAllObjects];
        
        selectedPositionForDropdown_2 = -1;
        [tableData_2 removeAllObjects];

        _Label_Greutate.text = @"Greutate";
        _Label_Administrare.text = @"Administrare";
//        [_Button_Greutate      setTitle:@"Greutate" forState:UIControlStateNormal];
//        [_Button_Administrari setTitle:@"Administrari" forState:UIControlStateNormal];
        
    }

    if(dropdownId == 2)
    {
        tableData = tableData_2;
    }

    if(dropdownId == 3)
    {
        tableData = tableData_3;
    }

    [dropDownTableView reloadData];
    dropDownTableView.hidden = NO;

}


- (void) initPage
{

    dropDownTableView = [[UITableView alloc] init];

    dropDownTableView.dataSource = self;
    dropDownTableView.delegate = self;

    dropDownTableView.hidden = YES;

    [self.view addSubview:dropDownTableView];

    // reset all fields to nil
    // dropdown table nil
    // dropdown table hidden

    selectedPositionForDropdown_1 = -1;
    selectedPositionForDropdown_2 = -1;
    selectedPositionForDropdown_2 = -1;
    currentDropdown = 0;
    
    [_Button_Medicament   setEnabled:YES];
    [_Button_Greutate     setEnabled:NO];
    [_Button_Administrari setEnabled:NO];
    [_Button_Calculeaza   setEnabled:NO];
    
    [self populate];
}
-(void) moveDropdownToButton//:( int ) buttonId
{
    int buttonId = currentDropdown;
    CGRect selectedButton ;
    dropDownTableView.hidden = YES;
    if(buttonId == 1)
    {
        NSLog(@"%f",_View_Medicament.frame.size.width);
        NSLog(@"%f",_Button_Medicament.frame.size.width);
        CGRect frame =  [_View_Medicament.superview convertRect:_View_Medicament.frame toView:self.view];//[self.view convertPoint:_Button_Medicament.frame.origin toView:nil];
        //CGRect frame = CGRectMake(origin.x, origin.y, _Button_Medicament.frame.size.width, _Button_Medicament.frame.size.height);
        selectedButton = frame;
        
//        selectedButton = _Button_Medicament.frame;
    }

    if(buttonId == 2)
    {
        //CGPoint origin =  [_View_Medicament convertPoint:_Button_Greutate.frame.origin toView:self.view];
        
        //CGRect frame = CGRectMake(origin.x, origin.y, _Button_Greutate.frame.size.width, _Button_Greutate.frame.size.height);
        CGRect frame =  [_View_Greutate.superview convertRect:_View_Greutate.frame toView:self.view];
        selectedButton = frame;
//        selectedButton = _Button_Greutate.frame;
    }

    if(buttonId == 3)
    {
        //CGPoint origin =  [self.view convertPoint:_Button_Administrari.frame.origin toView:nil];
        
        //CGRect frame = CGRectMake(origin.x, origin.y, _Button_Administrari.frame.size.width, _Button_Administrari.frame.size.height);
        CGRect frame =  [_View_Administrari.superview convertRect:_View_Administrari.frame toView:self.view];
        selectedButton = frame;
//        selectedButton = _Button_Administrari.frame;
    }

    CGRect tableFrame =CGRectMake(selectedButton.origin.x, selectedButton.origin.y + selectedButton.size.height, selectedButton.size.width, 200);

    [dropDownTableView setFrame: tableFrame];
}

- (void) getGreutate
{
    NSString * medName = [NSString stringWithFormat:@"%@", tableData_1[selectedPositionForDropdown_1]];
    [tableData_2 removeAllObjects];
    
    if(medTableData[medName]!=nil)
            {
                NSString * greutateString =medTableData[medName][@"greutate"];
                NSArray * items = [greutateString componentsSeparatedByString: @"#"];
                for(int i =0 ; i< items.count; i++)
                {
                    NSString * curent = [items objectAtIndex:i];
                    NSString * element = [NSString stringWithFormat:@"%@", curent];
                    [tableData_2 addObject:element];
                }
                
            }
}

- (void) getAdministrari
{
    NSString * medName = [NSString stringWithFormat:@"%@", tableData_1[selectedPositionForDropdown_1]];
    [tableData_3 removeAllObjects];
    
    if(medTableData[medName]!=nil)
    {
        NSString * administrariString =medTableData[medName][@"administrari"];
        NSArray * items = [administrariString componentsSeparatedByString: @"#"];
        for(int i =0 ; i< items.count; i++)
        {
            NSString * curent = [items objectAtIndex:i];
            NSString * element = [NSString stringWithFormat:@"%@", curent];
            [tableData_3 addObject:element];
        }
        
    }
}

-(void) hideDropdown
{
    self.ScrollView_scroll.scrollEnabled = true;
    
    currentDropdown = 0;
    dropDownTableView.hidden = YES;
    
    if(selectedPositionForDropdown_1 == -1)
    {
        [_Button_Greutate setEnabled:NO];
    }
    else
    {
        [_Button_Greutate setEnabled:YES];
        
        [self getGreutate];
        
    }
    
    if(selectedPositionForDropdown_2 == -1)
    {
        [_Button_Administrari setEnabled:NO];
    }
    else
    {
        [_Button_Administrari setEnabled:YES];
        
        [self getAdministrari];
//        NSString * medName = [NSString stringWithFormat:@"%@", tableData_1[selectedPositionForDropdown_1]];
//        if(medTableData[medName]!=nil)
//        {
//            int nr = [medTableData[medName][@"nr_administrari"] integerValue];
//            int initial = [medTableData[medName][@"administrare_0"] integerValue];
//            
//            [tableData_3 removeAllObjects];
//            
//            for(int i =0 ; i< nr; i++)
//            {
//                int curent = initial +i;
//                NSString * element = [NSString stringWithFormat:@"%d administrari", curent];
//                [tableData_3 addObject:element];
//            }
//            
//        }
    }
    
    if(selectedPositionForDropdown_1 != -1 && selectedPositionForDropdown_2 != -1 && selectedPositionForDropdown_3 != -1)
    {
        [_Button_Calculeaza setEnabled:YES];
    }
    else
    {
        [_Button_Calculeaza setEnabled:YES];
    }
}

- (void) Calculeaza
{
        //_TextView_Log.text =@"";
    
        NSString * medicamentSelected = [NSString stringWithFormat:@"%@", tableData_1[selectedPositionForDropdown_1]];
        NSString * greutateSelected = [NSString stringWithFormat:@"%@", tableData_2[selectedPositionForDropdown_2]];
        NSString * administrariSelected = [NSString stringWithFormat:@"%@", tableData_3[selectedPositionForDropdown_3]];
    
        _Label_Result_Medicament.text = medicamentSelected;
        _Label_Result_Greutate.text = greutateSelected;
        _Label_Result_Doze.text = administrariSelected;
    
        NSString * medName = [NSString stringWithFormat:@"%@", tableData_1[selectedPositionForDropdown_1]];
    
//        NSString *stringTemp = [NSString stringWithFormat:@"Medicament %@", tableData_1[selectedPositionForDropdown_1]];
//        _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//        stringTemp = [NSString stringWithFormat:@"Greutate %@", tableData_2[selectedPositionForDropdown_2]];
//        _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//        stringTemp = [NSString stringWithFormat:@"Doze %@", tableData_3[selectedPositionForDropdown_3]];
//        _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
    
        //Greutate * Doza
        float dozaZi;
        //dozaZi / nrDeAdministrari
        float dozaAdministrareMg;
        //dozaAdministrareMg * 5 / mg
        float dozaAdministrareMl;
    
        int greutate = [tableData_2[selectedPositionForDropdown_2] integerValue];
        int administrari = [tableData_3[selectedPositionForDropdown_3] integerValue];
        //NSString * doza = medTableData[medName][@"doza"];
    
        NSDictionary * med = medTableData[medName];
        NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
        [fmt setPositiveFormat:@"0.##"];
        NSNumberFormatter *fmt2 = [[NSNumberFormatter alloc] init];
        [fmt2 setPositiveFormat:@"0.#"];
    //    [fmt stringFromNumber:[NSNumber numberWithFloat:25.342]]
        if(med!=nil)
        {
            float doza = [med[@"doza"] floatValue];
            dozaZi = greutate * doza;
            NSString * dozaZiCalc =[NSString stringWithFormat:@"%@", [fmt stringFromNumber:[NSNumber numberWithFloat:dozaZi]] ];
            dozaAdministrareMg = dozaZi /administrari;
            NSString * dozaMgCalc =[NSString stringWithFormat:@"%@", [fmt stringFromNumber:[NSNumber numberWithFloat:lroundf(dozaAdministrareMg)]] ];
            float mg = [med[@"mg"] floatValue];
            float ml = [med[@"ml"] floatValue];
            dozaAdministrareMl = dozaAdministrareMg * ml /mg;
            float val0 =round(dozaAdministrareMl*10);
            float val = (val0) / 10;
            NSString * dozaMlCalc =[NSString stringWithFormat:@"%@", [fmt2 stringFromNumber:[NSNumber numberWithFloat:val]] ];
            
            _Label_Result_Doze24.text =dozaZiCalc;
            _Label_Result_Dozemg.text =dozaMgCalc;
            _Label_Result_Dozeml.text =dozaMlCalc;
            //stringTemp = [NSString stringWithFormat:@"Doza 24 ore (mg)  : %@", [fmt stringFromNumber:[NSNumber numberWithFloat:dozaZi]] ];
            //_TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
    
            //dozaAdministrareMg = dozaZi /administrari;
            //stringTemp = [NSString stringWithFormat:@"Doza Administrare (mg)  : %@", [fmt stringFromNumber:[NSNumber numberWithFloat:lroundf(dozaAdministrareMg)]] ];
            //_TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
    
//            float mg = [med[@"mg"] floatValue];
//            float ml = [med[@"ml"] floatValue];
//            dozaAdministrareMl = dozaAdministrareMg * ml /mg;
//            stringTemp = [NSString stringWithFormat:@"Doza Administrare (ml)  : %@", [fmt2 stringFromNumber:[NSNumber numberWithFloat:dozaAdministrareMl]] ];
            //_TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
    
        }
        else
        {
            //stringTemp = @" Modelul de calcul nu a fost adaugat in sistem! ";
            //_TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
        }
    }


- (IBAction)Action_Button_Info:(id)sender
{
  [self performSegueWithIdentifier: @"calculator_info" sender: self];
}

@end
////
////  ViewController.m
////  DropDownTest
////
////  Created by Alex on 24/03/15.
////  Copyright (c) 2015 Alex. All rights reserved.
////
//
//#import "ViewController.h"
//
//@interface ViewController ()
//
//@end
//
//@implementation ViewController
//
//NSMutableArray * tableData;
//
//NSMutableArray * tableData_1;
//NSMutableArray * tableData_2;
//NSMutableArray * tableData_3;
//
//NSMutableDictionary * medTableData;
//
//int currentDropdown = 0;
//
//int selectedPositionForDropdown_1 = -1;
//int selectedPositionForDropdown_2 = -1;
//int selectedPositionForDropdown_3 = -1;
//
//DropDownView * dropDown;
//
//UITableView * dropDownTableView;
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    [self initPage];
//    // Do any additional setup after loading the view, typically from a nib.
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//- (void) populate
//{
//    medTableData = [NSMutableDictionary new];
//    
//    //@"AMOXICILLINUM 125mg/5ml"
//    NSMutableDictionary * med1 = [NSMutableDictionary new];
//    [med1 setValue:@"AMOXICILLINUM 125mg/5ml" forKey:@"name"];
//    [med1 setValue:@"50" forKey:@"doza"];
//    [med1 setValue:@"125" forKey:@"mg"];
//    [med1 setValue:@"3" forKey:@"administrari"];
//    [medTableData setObject:med1 forKey:@"AMOXICILLINUM 125mg/5ml"];
//    
//    //@"AMOXICILLINUM 250mg/5ml"
//    NSMutableDictionary * med2 = [NSMutableDictionary new];
//    [med2 setValue:@"AMOXICILLINUM 250mg/5ml" forKey:@"name"];
//    [med2 setValue:@"50" forKey:@"doza"];
//    [med2 setValue:@"250" forKey:@"mg"];
//    [med2 setValue:@"3" forKey:@"administrari"];
//    [medTableData setObject:med2 forKey:@"AMOXICILLINUM 250mg/5ml"];
//    
//    
//    
//    tableData_1 = [NSMutableArray arrayWithObjects:
//                   @"AMOXICILLINUM 125mg/5ml",
//                   @"AMOXICILLINUM 250mg/5ml",
//                   @"AMOXICILLINUM + ACIDUM CLAVULANICUM 125mg+31,25/5ml",
//                   @"AMOXICILLINUM + ACIDUM CLAVULANICUM 156,25mg/5ml",
//                   @"AMOXICILLINUM + ACIDUM CLAVULANICUM 250mg+62,5mg/5ml",
//                   @"AMOXICILLINUM + ACIDUM CLAVULANICUM 400mg+57mg/5ml",
//                   @"AMOXICILLINUM + ACIDUM CLAVULANICUM 600 mg/42,9mg/5ml",
//                   @"CEFALEXINUM 125mg/5ml",
//                   @"CEFALEXINUM 250mg/5ml",
//                   @"CEFADROXILUM 125mg/5ml",
//                   @"CEFADROXILUM 250mg/5ml",
//                   @"CEFUROXIMUM 125mg/5ml",
//                   @"CEFACLORUM 125mg/5ml",
//                   @"CEFACLORUM 125mg/5ml",
//                   @"SULFAMETHOXAZOLUM + TRIMETHOPRIMUM 25mg/5mg/ml",
//                   @"ERYTHROMYCINUM 200mg/5ml",
//                   @"CLARITHROMYCINUM 125mg/5ml",
//                   @"CLARITHROMYCINUM 250mg/5ml", nil];
//    
//    //    tableData_2 = [NSMutableArray arrayWithObjects:
//    //                   @"1 kg",
//    //                   @"2 kg",
//    //                   @"3 kg",
//    //                   @"4 kg",
//    //                   nil];
//    
//    tableData_2 = [NSMutableArray new];
//    
//    for(int i = 3; i < 51;i++)
//    {
//        NSString * text = [NSString stringWithFormat:@"%d kg",i];
//        [tableData_2 addObject: text];
//    }
//    
//    
//    tableData_3 = [NSMutableArray arrayWithObjects:
//                   @"2 administrari",
//                   @"3 administrari",
//                   @"4 administrari",
//                   nil];
//    
//}
//
//- (void) showDropDown //:(int ) dropdownId
//{
//    int dropdownId = currentDropdown;
//    if(dropdownId == 1)
//    {
//        tableData = tableData_1;
//    }
//    
//    if(dropdownId == 2)
//    {
//        tableData = tableData_2;
//    }
//    
//    if(dropdownId == 3)
//    {
//        tableData = tableData_3;
//    }
//    
//    [dropDownTableView reloadData];
//    dropDownTableView.hidden = NO;
//    
//}
//
//
//- (void) initPage
//{
//    
//    dropDownTableView = [[UITableView alloc] init];
//    
//    dropDownTableView.dataSource = self;
//    dropDownTableView.delegate = self;
//    
//    dropDownTableView.hidden = YES;
//    
//    [self.view addSubview:dropDownTableView];
//    
//    // reset all fields to nil
//    // dropdown table nil
//    // dropdown table hidden
//    
//    selectedPositionForDropdown_1 = -1;
//    selectedPositionForDropdown_2 = -1;
//    selectedPositionForDropdown_2 = -1;
//    currentDropdown = 0;
//    
//    _TableView_DropDown.hidden = YES;
//    
//    [self populate];
//}
//
//-(void) moveDropdownToButton//:( int ) buttonId
//{
//    int buttonId = currentDropdown;
//    CGRect selectedButton ;
//    dropDownTableView.hidden = YES;
//    if(buttonId == 1)
//    {
//        selectedButton = _Button_btn1.frame;
//    }
//    
//    if(buttonId == 2)
//    {
//        selectedButton = _Button_btn2.frame;
//    }
//    
//    if(buttonId == 3)
//    {
//        selectedButton = _Button_btn3.frame;
//    }
//    
//    CGRect tableFrame =CGRectMake(selectedButton.origin.x, selectedButton.origin.y + selectedButton.size.height, selectedButton.size.width, 200);
//    
//    [dropDownTableView setFrame: tableFrame];
//}
//
//-(void) hideDropdown
//{
//    currentDropdown = 0;
//    dropDownTableView.hidden = YES;
//}
//
//- (IBAction)Action_Button_btn1:(id)sender
//{
//    if(currentDropdown == 1)
//    {
//        [self hideDropdown];
//    }
//    else
//    {
//        currentDropdown = 1;
//        [self moveDropdownToButton];
//        [self showDropDown];
//    }
//}
//- (IBAction)Action_Button_btn2:(id)sender
//{
//    if(currentDropdown == 2)
//    {
//        [self hideDropdown];
//    }
//    else
//    {
//        currentDropdown = 2;
//        [self moveDropdownToButton];
//        [self showDropDown];
//    }
//}
//
//- (IBAction)Action_Button_btn3:(id)sender
//{
//    if(currentDropdown == 3)
//    {
//        [self hideDropdown];
//    }
//    else
//    {
//        currentDropdown = 3;
//        [self moveDropdownToButton];
//        [self showDropDown];
//    }
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return tableData.count;
//}
//
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"DropDownCell"];
//    if (!cell)
//    {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"DropDownCell"];
//    }
//    cell.textLabel.text = [tableData objectAtIndex: indexPath.row];
//    [cell.textLabel sizeToFit];
//    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
//    return cell;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(currentDropdown == 1)
//    {
//        selectedPositionForDropdown_1 = indexPath.row;
//        NSString * title = [tableData objectAtIndex:indexPath.row];
//        [_Button_btn1 setTitle:title forState:UIControlStateNormal];
//        [self hideDropdown];
//    }
//    
//    if(currentDropdown == 2)
//    {
//        selectedPositionForDropdown_2 = indexPath.row;
//        NSString * title = [tableData objectAtIndex:indexPath.row];
//        [_Button_btn2 setTitle:title forState:UIControlStateNormal];
//        [self hideDropdown];
//    }
//    
//    if(currentDropdown == 3)
//    {
//        selectedPositionForDropdown_3 = indexPath.row;
//        NSString * title = [tableData objectAtIndex:indexPath.row];
//        [_Button_btn3 setTitle:title forState:UIControlStateNormal];
//        [self hideDropdown];
//    }
//}
//
//
//- (IBAction)Action_Button_Calculate:(id)sender
//{
//    if(selectedPositionForDropdown_1!=-1 && selectedPositionForDropdown_2 != -1 && selectedPositionForDropdown_3 !=-1)
//    {
//        _TextView_Log.text = @"";
//        
//        [self calculateStuff];
//    }
//    
//}
//
//-(void) calculateStuff
//{
//    NSString * medName = [NSString stringWithFormat:@"%@", tableData_1[selectedPositionForDropdown_1]];
//    NSString *stringTemp = [NSString stringWithFormat:@"Medicament %@", tableData_1[selectedPositionForDropdown_1]];
//    _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//    stringTemp = [NSString stringWithFormat:@"Greutate %@", tableData_2[selectedPositionForDropdown_2]];
//    _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//    stringTemp = [NSString stringWithFormat:@"Doze %@", tableData_3[selectedPositionForDropdown_3]];
//    _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//    
//    //Greutate * Doza
//    float dozaZi;
//    //dozaZi / nrDeAdministrari
//    float dozaAdministrareMg;
//    
//    //dozaAdministrareMg * 5 / mg
//    float dozaAdministrareMl;
//    
//    int greutate = selectedPositionForDropdown_2 + 3;
//    int administrari = selectedPositionForDropdown_3 + 2;
//    //NSString * doza = medTableData[medName][@"doza"];
//    
//    NSDictionary * med = medTableData[medName];
//    
//    if(med!=nil)
//    {
//        float doza = [med[@"doza"] floatValue];
//        dozaZi = greutate * doza;
//        stringTemp = [NSString stringWithFormat:@"Doza 24 ore : %f", dozaZi ];
//        _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//        
//        dozaAdministrareMg = dozaZi /administrari;
//        stringTemp = [NSString stringWithFormat:@"Doza Administrare (mg)  : %f", dozaAdministrareMg ];
//        _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//        
//        float mg = [med[@"mg"] floatValue];
//        dozaAdministrareMl = dozaAdministrareMg * 5 /mg;
//        stringTemp = [NSString stringWithFormat:@"Doza Administrare (ml)  : %f", dozaAdministrareMl ];
//        _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//        
//    }
//    else
//    {
//        stringTemp = @" Modelul de calcul nu a fost adaugat in sistem! ";
//        _TextView_Log.text = [NSString stringWithFormat:@"%@ \n %@", _TextView_Log.text, stringTemp];
//    }
//}
//
//
//
//
//
//
//@end
